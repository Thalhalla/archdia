# archdia

[Thalhalla Arch Linux Repository for Dia](https://thalhalla.gitlab.io/archdia/) 

Add this to your `/etc/pacman.conf` and `pacman -Sy`

```
[archdia]
SigLevel = Optional TrustedOnly
Server = https://thalhalla.gitlab.io/$repo/$arch
```

And then install any of the packages listed below e.g. `pacman -S dia-shapes`

dia-shapes
