import * as React from "react"

// styles
const pageStyles = {
  color: "#232129",
  padding: 96,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
}
const headingAccentStyles = {
  color: "#663399",
}
const paragraphStyles = {
  marginBottom: 48,
}
const codeStyles = {
  color: "#8A6534",
  padding: 4,
  backgroundColor: "#FFF4DB",
  fontSize: "1.25rem",
  borderRadius: 4,
}
const listStyles = {
  marginBottom: 96,
  paddingLeft: 0,
}
const listItemStyles = {
  fontWeight: 300,
  fontSize: 24,
  maxWidth: 560,
  marginBottom: 30,
}

const linkStyle = {
  color: "#8954A8",
  fontWeight: "bold",
  fontSize: 16,
  verticalAlign: "5%",
}

const docLinkStyle = {
  ...linkStyle,
  listStyleType: "none",
  marginBottom: 24,
}

const descriptionStyle = {
  color: "#232129",
  fontSize: 14,
  marginTop: 10,
  marginBottom: 0,
  lineHeight: 1.25,
}

const docLink = {
  text: "AUR packages built by this repo",
  url: "https://aur.archlinux.org/",
  color: "#8954A8",
}

const badgeStyle = {
  color: "#fff",
  backgroundColor: "#088413",
  border: "1px solid #088413",
  fontSize: 11,
  fontWeight: "bold",
  letterSpacing: 1,
  borderRadius: 4,
  padding: "4px 6px",
  display: "inline-block",
  position: "relative",
  top: -2,
  marginLeft: 10,
  lineHeight: 1,
}

// data
const links = [
  {
    text: "brave-bin",
    url: "https://aur.archlinux.org/packages/brave-bin",
    badge: true,
    description:
      "Brave Browser",
    color: "#E95800",
  },
  {
    text: "tintin",
    url: "https://aur.archlinux.org/packages/tintin",
    badge: true,
    description:
      "Tintin MUD client",
    color: "#E95800",
  },
  {
    text: "stockfish",
    url: "https://aur.archlinux.org/packages/stockfish",
    badge: true,
    description:
      "Stockfish chess engine",
    color: "#E95800",
  },
  {
    text: "autokey-qt",
    url: "https://aur.archlinux.org/packages/autokey-qt",
    badge: true,
    description:
      "autokey-qt",
    color: "#E95800",
  },
  {
    text: "dropbox",
    url: "https://aur.archlinux.org/packages/dropbox",
    description:
      "Dropbox",
    color: "#E95800",
  }
]

// markup
const IndexPage = () => {
  return (
    <main style={pageStyles}>
      <title>Thalhalla Arch Repo</title>
      <h1 style={headingStyles}>
        Thalhalla
        <br />
      </h1>
      <h3 style={headingStyles}>
        <span style={headingAccentStyles}>— Arch Linux Repository </span>
      </h3>
      <p style={paragraphStyles}>
        Add this to your /etc/pacman.conf and <code style={codeStyles}>pacman -Sy</code>
      </p>
      <p>
        <code style={codeStyles}>[archdia]</code>
      </p>
      <p>
        <code style={codeStyles}>SigLevel = Optional TrustedOnly</code>
      </p>
      <p>
        <code style={codeStyles}>Server = https://thalhalla.gitlab.io/$repo/$arch</code>
      </p>
      <p style={paragraphStyles}>
        And then install any of the packages listed below e.g. <code style={codeStyles}>pacman -S brave-bin</code>
      </p>
      <p style={paragraphStyles}>
        <span role="img" aria-label="Sunglasses smiley emoji">
          😎
        </span>
      </p>
      <ul style={listStyles}>
        <li style={docLinkStyle}>
          <a
            style={linkStyle}
            href={`${docLink.url}?utm_source=starter&utm_medium=start-page&utm_campaign=minimal-starter`}
          >
            {docLink.text}
          </a>
        </li>
        {links.map(link => (
          <li key={link.url} style={{ ...listItemStyles, color: link.color }}>
            <span>
              <a
                style={linkStyle}
                href={`${link.url}?utm_source=starter&utm_medium=start-page&utm_campaign=minimal-starter`}
              >
                {link.text}
              </a>
              {link.badge && (
                <span style={badgeStyle} aria-label="New Badge">
                  NEW!
                </span>
              )}
              <p style={descriptionStyle}>{link.description}</p>
            </span>
          </li>
        ))}
      </ul>
      <h4 style={headingStyles}> This <a href="https://gitlab.com/Thalhalla/archdia/">repo</a> is run on gitlab pipelines.</h4>
    </main>
  )
}

export default IndexPage
