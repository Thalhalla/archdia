FROM thalhalla/archbuilder:latest

ARG TRAVIS_REPO_SLUG
ARG TRAVIS_BUILD_ID
ARG TRAVIS_TAG
ARG AUR_PAGER

USER pkguser
WORKDIR /home/pkguser

COPY pkguser /home/pkguser/
RUN pacman -Sy --noconfirm archlinux-keyring; gpg --recv-keys 04C367C218ADD4FF
RUN bash build.sh
